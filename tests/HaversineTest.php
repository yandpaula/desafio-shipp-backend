<?php

namespace Tests;

use Tests\TestCase;
use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;

class HaversineTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * Testing the get products with admin.
     *
     * @return void
     */
    public function testGetEstablishmentsWithoutPosition()
    {
        $service = new \App\Services\EstablishmentService();

        $distance = $service->haversine(40.7486, 42.665813, -73.9864, -73.772871);
        $this->assertEquals(213.9, number_format($distance, 1, '.', ''));

        $distance = $service->haversine(40.7486, -15.794368, -73.9864, -47.995020);
        $this->assertEquals(6835, number_format($distance, 0, '.', ''));

        $distance = $service->haversine(40.7486, 50.548585, -73.9864, 9.704455);
        $this->assertEquals(6244, number_format($distance, 0, '.', ''));
    }
}