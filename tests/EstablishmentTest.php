<?php

namespace Tests;

use Tests\TestCase;
use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;

class EstablishmentTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * Testing the get products with admin.
     *
     * @return void
     */
    public function testGetEstablishmentsWithoutPosition()
    {
        $response = $this->call('GET', 'v1/stores/');

        $this->assertEquals(422, $response->status());
    }

    /**
     * Testing the get products with admin.
     *
     * @return void
     */
    public function testGetEstablishmentsWithPosition()
    {
        $this->json('GET', 'v1/stores/', [
            'latitude' => '42.681478',
            'longitude' => '-73.791629'
        ])->seeJson([
            'success' => true
        ]);
    }

    /**
     * Testing the get products with admin.
     *
     * @return void
     */
    public function testMiddlewareValidation()
    {    
        $this->json('GET', 'v1/stores/', [
            'latitude' => '42.681478',
            'lngitude' => '-73.791629'
        ]);

        $this->assertEquals(422, $this->response->status());
            
        $this->json('GET', 'v1/stores/', [
            'latitude' => '42.681478',
            'longitude' => '-73.7a91629'
        ]);

        $this->assertEquals(422, $this->response->status());

        $this->json('GET', 'v1/stores/', []);

        $this->assertEquals(422, $this->response->status());
    }

}