<?php

namespace App\Console\Commands;

use App\Models\Establishment;
use App\Models\EstablishmentAdress;
use Exception;
use Illuminate\Console\Command;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;

class LoadEstablishmentsCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = "load:establishments";

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "Carrega os estabelecimentos do arquivo stores.csv na raiz do projeto.";


    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            $this->info("Iniciando processo de leitura e inserção dos estabelecimentos");

            $storesPath = __DIR__ . '/../../../stores.csv';
            
            if (!file_exists($storesPath)) {
                $this->info("arquivo não encontrado");
                die();
            }
            
            $establishment = Establishment::first();
            
            if (!empty($establishment)) {
                $this->info("arquivo já foi carregado");
                die();
            }

            $handle = fopen($storesPath, 'r');
            $row = 0;
            $this->info('Aguarde enquanto os estabelecimentos são carregados');
            
            DB::beginTransaction();
            
            while ($line = fgetcsv($handle, 2000, ",")) {
                if ($row++ == 0) {
                    continue;
                }
                
                $line[14] = $this->getStoreLocation($line[14]);
                
                $establishmentData = [
                    'dba_name' => $line[5],
                    'entity_name' => $line[4],
                    'establishment_type' => $line[3],
                    'license_number' => $line[1],
                    'operation_type' => $line[2]
                ];

                $establishment = $this->saveEstablishment($establishmentData);

                $adress = [
                    'establishment_id' => $establishment->id,
                    'county' => $line[0],
                    'street_number' => $line[6],
                    'street_name' => $line[7],
                    'adress2' => $line[8],
                    'adress3' => $line[9],
                    'zipcode' => $line[12],
                    'city' => $line[10],
                    'state' => $line[11],
                    'square_footage' => $line[13],
                    'location' => $line[14]
                ];
                $this->info($row);
                $this->saveEstablishmentAdress($adress);
            }
            fclose($handle);
            DB::commit();
            $this->info("processo finalizado");
        } catch (Exception $e) {
            DB::rollback();
            Log::alert('falha ao carregar estabelecimentos: ' . $e->getMessage());
            $this->error("An error occurred");
        }
    }

    /**
     * Remove dados desnecessários da string e retorna latitude e longitude
     *
     * @param string $locations
     * @return Array
     */
    public function getStoreLocation(string $locations)
    {
        try {
            $location = explode(',', str_replace(['{', '}'], '', $locations));
            $longitude = explode(':', str_replace("'", '', $location[0]));
            $latitude = explode(':', str_replace("'", '', $location[count($location) - 1]));

            if ($longitude[0] == 'longitude') {
                return ['lat' => $latitude[1], 'lon' => $longitude[1]];
            } else {
                return [];
            }
        } catch (\Throwable $th) {
            throw $th;
        }
    }

    /**
     * Insere o estabelicimento no banco
     *
     * @param array $storeData
     * @return Establishment
     */
    public function saveEstablishment(array $storeData)
    {
        try {
            $establishment = new Establishment();
            
            $establishment->dba_name = trim($storeData['dba_name']);
            $establishment->entity_name = trim($storeData['entity_name']);
            $establishment->establishment_type = trim($storeData['establishment_type']);
            $establishment->license_number = trim($storeData['license_number']);
            $establishment->operation_type = trim($storeData['operation_type']);
            $establishment->save();

            return $establishment;
        } catch (QueryException $q) {
            Log::critical('Falha em saveEstablishment: ' . $q->getMessage());
        } catch (Exception $e) {
            Log::critical('Falha em saveEstablishment: ' . $e->getMessage());
        }
    }

    /**
     * Insere o estabelicimento no banco
     *
     * @param array $storeData
     * @return Establishment
     */
    public function saveEstablishmentAdress(array $adressData)
    {
        try {
            $adress = new EstablishmentAdress();
            $adress->establishment_id = $adressData['establishment_id'];
            $adress->county = trim($adressData['county']);
            $adress->street_number = trim($adressData['street_number']);
            $adress->street_name = trim($adressData['street_name']);
            $adress->adress2 = trim($adressData['adress2']);
            $adress->adress3 = trim($adressData['adress3']);
            $adress->zipcode = trim($adressData['zipcode']);
            $adress->city = trim($adressData['city']);
            $adress->state = trim($adressData['state']);
            $adress->square_footage = $adressData['square_footage'];
            if (isset($adressData['location']) && !empty($adressData['location'])) {
                $adress->latitude = trim($adressData['location']['lat']);
                $adress->longitude = trim($adressData['location']['lon']);
            }
            $adress->save();
        } catch (QueryException $q) {
            Log::critical('Falha em saveEstablishment: ' . $q->getMessage());
        } catch (Exception $e) {
            Log::critical('Falha em saveEstablishment: ' . $e->getMessage());
        }
    }
}
