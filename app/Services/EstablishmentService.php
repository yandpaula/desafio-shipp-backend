<?php

namespace App\Services;

use App\Models\Establishment;
use App\Models\EstablishmentAdress;
use Exception;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class EstablishmentService
{
    /**
     * Retorna as lojas mais proximas
     *
     * @param array $position
     * @return void
     */
    public function getNearestStores(array $position)
    {
        try {
            $stores = DB::select("
                SELECT
                    EA.establishment_id AS id,
                    EA.latitude AS lat,
                    EA.longitude AS lon,
                    ((? - EA.latitude)*(? - EA.latitude)) + ((? - EA.longitude)*(? - EA.longitude)) AS distance
                FROM establishment_adresses EA
                WHERE
                    EA.latitude IS NOT NULL AND
                    EA.longitude IS NOT NULL AND
                    distance < 0.005
                ORDER BY distance ASC
            ", [$position['latitude'], $position['latitude'], $position['longitude'], $position['longitude']]);

            foreach ($stores as $key => $store) {
                $distancia = $this->haversine($position['latitude'], $store->lat, $position['longitude'], $store->lon);
                if ($distancia <= 6.5 && $distancia > 0) {
                    $distancias[] = $distancia;
                    $store->distance = $distancia;
                }
            }
            
            sort($distancias);
            $sortedStores = $this->getSortedStores($stores, $distancias);
            
            return $sortedStores;
        } catch (QueryException $q) {
            Log::critical('Falha em getNearestStore: ' . $q->getMessage());
            throw $q;
        } catch (Exception $e) {
            Log::critical('Falha em getNearestStore: ' . $e->getMessage());
            throw $e;
        }
    }

    /**
     * Ordena as lojas
     *
     * @param array $stores
     * @param array $positon
     * @return void
     */
    public function getSortedStores(array $stores, array $distancias)
    {
        try {
            $orderedStores = [];
            foreach ($distancias as $key => $distancia) {
                foreach ($stores as $key => $store) {
                    if ($store->distance == $distancia) {
                        $establishment = Establishment::leftJoin('establishment_adresses', 'establishment_adresses.establishment_id', 'id')
                        ->where('id', $store->id)
                        ->first();

                        $establishment->distance = $store->distance . 'km';
                        $orderedStores[] = $establishment;
                        unset($stores[$key]);
                        continue;
                    }
                }
                unset($distancias[$key]);
            }
            return $orderedStores;
        } catch (Exception $e) {
            Log::critical('Falha em getNearestStore: ' . $e->getMessage());
            throw $e;
        }
    }

    /**
     * Retorna a distância entre dois pontos em uma esfera
     *
     * @param [type] $lat1
     * @param [type] $lat2
     * @param [type] $lon1
     * @param [type] $lon2
     * @return array
     */
    public function haversine($lat1, $lat2, $lon1, $lon2)
    {
        try {
            $lat1 = deg2rad($lat1);
            $lat2 = deg2rad($lat2);
            $lon1 = deg2rad($lon1);
            $lon2 = deg2rad($lon2);

            $latD = $lat2 - $lat1;
            $lonD = $lon2 - $lon1;

            $dist = 2 * asin(sqrt(pow(sin($latD / 2), 2) +
            cos($lat1) * cos($lat2) * pow(sin($lonD / 2), 2)));
            $dist = $dist * 6371;

            return number_format($dist, 2, '.', '');
        } catch (\Throwable $th) {
            throw $th;
        }
    }
}
