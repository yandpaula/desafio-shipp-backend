<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller as Controller;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class ResponseController extends Controller
{
    /**
     * metodo de retorno de sucesso
     *
     * @return \Illuminate\Http\Response
     */
    public function sendResponse($data, $message)
    {
        try {
            $response = [
                'success' => true,
                'data'    => $data,
                'message' => $message,
            ];

            return response()->json($response, 200);
        } catch (Exception $e) {
            Log::critical('Falha em sendResponse: ' . $e->getMessage());
            return $this->sendError('Desculpe algo deu errado', [], 500);
        }
    }


    /**
     * Metodo de resposta de erro.
     *
     * @return \Illuminate\Http\Response
     */
    public function sendError($error, $errorMessages = [], $code = 404)
    {
        try {
            $response = [
                'success' => false,
                'message' => $error,
            ];
    
            if (!empty($errorMessages)) {
                $response['data'] = $errorMessages;
            }
    
            return response()->json($response, $code);
        } catch (Exception $e) {
            Log::critical('Falha em sendError: ' . $e->getMessage());
            return response()->json(['Desculpe algo deu errado'], 500, $headers);
        }
    }
}
