<?php

namespace App\Http\Controllers;

use App\Services\EstablishmentService;
use Exception;
use Illuminate\Support\Facades\Log;

class EstablishmentController extends ResponseController
{
    /**
     * Serviço de estabelecimentos
     *
     * @var EstablishmentService
     */
    private $service;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(EstablishmentService $service)
    {
        $this->service = $service;
    }

    /**
     * Retorna todas as lojas em um raio de no máximo 6.5 km
     * de acordo com a latitude e longitude informada
     *
     * @return void
     */
    public function index()
    {
        try {
            $position = json_decode(request()->getContent(), true);

            $stores = $this->service->getNearestStores($position);
            
            return $this->sendResponse($stores, 'Lojas encontradas');
        } catch (Exception $e) {
            Log::critical('Falha ao retornar as lojas: ' . $e->getMessage());
            return $this->sendError('Desculpe algo deu errado', [], 500);
        }
    }
}
