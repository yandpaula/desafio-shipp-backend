<?php

namespace App\Http\Middleware;

use App\Models\Log;
use Illuminate\Support\Facades\Log as LaravelLog;
use Closure;
use Exception;

class LogStoresResponse
{
    public function handle($request, Closure $next)
    {
        try {
            $response = $next($request);
            $position = json_decode($request->getContent(), true);
            $resArray = json_decode($response->getContent(), true);
            $total = isset($resArray['data']) ? count($resArray['data']) + 1 : 0;
            
            $log = new Log();
            $log->position = $position['latitude'] . ',' . $position['longitude'];
            $log->establishments = $total;
            $log->status_code = $response->status();
            $log->save();

            return $response;
        } catch (Exception $e) {
            LaravelLog::critical('Erro no middleware de log: ' . $e->getMessage());
            return $response;
        }
    }
}
