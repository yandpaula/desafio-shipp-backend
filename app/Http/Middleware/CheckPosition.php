<?php

namespace App\Http\Middleware;

use Closure;
use Validator;

class CheckPosition
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $position = (json_decode($request->getContent(), true));
        
        if (is_null($position)) {
            return response()->json(['Nenhuma posição passada'], 422);
        }
        $validator = Validator::make($position, [
            'latitude' => 'required|numeric',
            'longitude' => 'required|numeric',
        ]);

        if ($validator->fails()) {
            return response()->json([$validator->errors()], 422);
        }
        
        return $next($request);
    }
}
