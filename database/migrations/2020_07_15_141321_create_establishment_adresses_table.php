<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEstablishmentAdressesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('establishment_adresses', function (Blueprint $table) {
            $table->foreignId('establishment_id')->constrained();
            $table->string('county')->nullable();
            $table->string('street_number')->nullable();
            $table->string('street_name')->nullable();
            $table->string('adress2')->nullable();
            $table->string('adress3')->nullable();
            $table->string('zipcode');
            $table->string('city');
            $table->string('state');
            $table->integer('square_footage');
            $table->float('latitude', 10, 2)->nullable();
            $table->float('longitude', 10, 2)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('establishment_adresses');
    }
}
